public class Sesion1_Datos {
    public static void main(String[] args) {
        Sesion1_Datos S1 = new Sesion1_Datos();      
        S1.tipos_datos();
        S1.operadores();
        S1.lectura_datos();
    }

    public void tipos_datos(){
        /*TIPOS DE DATOS*/
        
        //Enteros
        byte numero1 = 127;
        short numero2 = 32767;
        int numero3 = 2147483647;
        long numero4 = 922337203685477580L;
        
        //Reales
        float numero5 = 3.4028235E38f;
        double numero6 = 1.7976931348623157E308;
        
        //Caracter
        char texto1 = 'H';
        String texto2 = "Hola";
        
        //Lógicos
        boolean valida1 = true;
        boolean valida2 = false;
        
        
        System.out.println("NUMEROS ENTEROS");
        System.out.println("byte:"+numero1+" - short:"+numero2+" - int:"+numero3+" - long:"+numero4+"\n");
        System.out.println("NUMEROS REALES");
        System.out.println("float:"+numero5+" - double:"+numero6+"\n");
        System.out.println("CARACTERES");
        System.out.println("char:"+texto1+" - String:"+texto2+"\n");
        System.out.println("LOGICOS O BOOLEANOS");
        System.out.println(valida1+" - "+valida2+"\n");

        //Identificar valor maximo y minimo de cada tipo de dato númerico
        System.out.println("byte\t" + Byte.MIN_VALUE + "\t" + Byte.MAX_VALUE);
        System.out.println("short\t" + Short.MIN_VALUE + "\t" + Short.MAX_VALUE);
        System.out.println("int\t" + Integer.MIN_VALUE + "\t" + Integer.MAX_VALUE);
        System.out.println("long\t" + Long.MIN_VALUE + "\t" + Long.MAX_VALUE);
        System.out.println("float\t" + Float.MIN_VALUE + "\t" + Float.MAX_VALUE);
        System.out.println("double\t" + Double.MIN_VALUE + "\t" + Double.MAX_VALUE);
    }
    
    public void operadores(){
        System.out.println("\nOPERADORES ARITMÉTICOS");
        
        //Suma (+)
        int suma = 4 + 5;
        System.out.println("4+5 = "+suma);
        
        //Resta (-)
        int resta = 8 - 3;
        System.out.println("8-3 = "+resta);
        
        //Multiplicacion (*)
        int multiplica = 5 * 8;
        System.out.println("5*8 = "+multiplica);
        
        //División de dos valores enteros (/) - La division de 2 numeros enteros en java siempre dará un numero entero
        double  divide = 7/2;
        System.out.println("13/2 = "+divide);
        
        //División de dos valores con resultado decimal (/)
        double  dividedeciamles = (double)7/ 2;
        System.out.println("13/2 = "+dividedeciamles);
        /**
        * El casting es un procedimiento para transformar una variable primitiva de un tipo a otro, o transformar un objeto de una clase a otra clase siempre 
        * y cuando haya una relación de herencia entre ambas (este último casting es el más importante y se verá más adelante).
        */
        
        //Módulo (%) - El residuo de la división de dos números
        int modulo = 26 % 3;
        System.out.println("26%3 = "+modulo);
        
        
        System.out.println("\nOPERADORES DE ASIGNACION");
        
        //Asignación (variable = valor)
        int x = 8;
        System.out.println("x="+x);
        
        //Operador de incremento ++
        x++; //Equivale a x = x + 1
        System.out.println("x++ --> "+x);
        
        //Operador de decremento --
        x--; //Equivale a x = x - 1
        System.out.println("x-- --> "+x);
        
        //Asignación con suma +=
        x += 2; //Equivale a suma = suma + 2
        System.out.println("x+=2 --> "+x);
        
        //Asignación con resta -=
        x -= 4; //Equivale a resta = resta - 4
        System.out.println("x-=4 --> "+x);
        
        //Asignación con multiplicación *=
        x *= 6; //Equivale a multiplica = multiplica * 4
        System.out.println("x*=6 --> "+x);
        
        //Asignación con división /=
        x /= 3; //Equivale a divide = divide / 3
        System.out.println("x/=3 --> "+x);
        
        //Asignación con modulo %=
        x %= 2; //Equivale a modulo = modulo % 2
        System.out.println("x%=2 --> "+x);
        
        
        System.out.println("\nOPERADORES DE IGUALDAD Y RELACIONES");
        int a= 3, b= 5;
        System.out.println("a="+a+"\tb="+b+"\n");
        
        //== devuelve verdadero si dos valores son iguales
        System.out.println("(a == b) --> "+(a == b));
        
        //!= devuelve verdadero si dos valores son diferentes
        System.out.println("(a != b) --> "+(a != b));
        
        //> mayor que, devuelve verdadero si el primero operador es estrictamente mayor que el segundo operador
        System.out.println("(a > b)  --> "+(a > b));
        
        //< menor que, devuelve verdadero si el primero operador es estrictamente menor que el segundo operador
        System.out.println("(a < b)  --> "+(a < b));
        
        //>= mayor o igual
        System.out.println("(a >= b) --> "+(a >= b));
                
        //<= menor o igual
        System.out.println("(a <= b) --> "+(a <= b));
        
        
        //Conjunción : a && b

        //Disyunción: a || b
        
        System.out.println("\nOPERADORES LOGICOS\n");
        
        //Negación : !a
        System.out.println("!(a == b) --> " + (!(a == b)));
        
        //Conjunción : a && b
        System.out.println("(a != b) && (a < b) --> " + ((a != b) && (a < b)));

        //Disyunción: a || b
        System.out.println("(a != b) || (a < b) --> " + ((a <= b) || (a >= b)));

    }
    
    public void lectura_datos(){
        System.out.println("\nENTRADA Y SALIDA DE DATOS\n");
        
        
        //Definir tipo de dato a leer
        byte byte1=1;
        short short1;
        int int1;
        
        
        System.out.print("\nIngrese un numero byte: "); 
        byte1 = Leer.datoByte();  
        System.out.print("Ingrese un numero short: "); 
        short1 = Leer.datoShort();
        System.out.print("Ingrese un numero entero: "); 
        int1 = Leer.datoInt();
        
    }
}
